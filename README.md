## Assignment 1:  The Computer Store


## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)

## Background

This is a simple javascript web program simulating computer web store

This is the first assignment of the frontend part of the [Noroff](https://www.noroff.no/en/) Java Full-Stack developer course.
The task was to create a simple web store frontend, where user can buy laptops, work, loan and bank money.


### Usage

- Work: User can get pay by cliking the work button. The pay from each click is 100 euros. 
- Bank: User can move his payment to the bank
- Get a loan: User can get loan max twice his balance. Only one loan is allowed at a time
- Repay Loan: Users can use their pay to repay current loan.
- Buy: Users can use the buy button to buy the laptop they want if they have enough money on their balance.

Users can use the slider to inspect different laptops available at the store.


## Install

Clone repository `git clone`

## Maintainers

[@Laura Byman](https://gitlab.com/Laura_Byman)
