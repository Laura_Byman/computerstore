import { addToBalance, payLoan, toCurrency } from './bank.js'

const payElement = document.getElementById("pay")
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const repayElement = document.getElementById("repayLoan");

let salary = 100;
let payBalance = 0;
document.getElementById("repayLoan").hidden = true;

payElement.innerHTML = "Pay: " + toCurrency(payBalance)

 const handleBank = () => {
     addToBalance(payBalance);
     payBalance = 0;
     payElement.innerHTML = "Pay: " + toCurrency(payBalance)
}

const handleRepay = () => {
    payLoan(payBalance);
    payBalance = 0;
    payElement.innerHTML = "Pay: " + toCurrency(payBalance)
}


const handleWork = () => {
    payBalance += salary
    payElement.innerHTML = "Pay: " + toCurrency(payBalance)
}

workElement.addEventListener("click", handleWork)
bankElement.addEventListener("click", handleBank)
repayElement.addEventListener("click", handleRepay)