import { toCurrency, payFromBalance } from './bank.js'

const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const imageElement = document.getElementById("computerImage");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyElement = document.getElementById("buy");

let laptops = [];
let selectedLaptop = null;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => laptops=data)
.then(laptops => addLaptopsToStore(laptops))

const addLaptopsToStore = (laptops) => {
    laptops.forEach(laptop => addLaptopToStore(laptop)) 
    
    if(laptops.length > 0) {
        selectedLaptop = laptops[0]
        showLaptopInfo(laptops[0])
    }
    
}

const addLaptopToStore = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopStoreChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex]
    showLaptopInfo(selectedLaptop)

}

const showLaptopInfo = (laptop) => {
    laptop.specs.forEach(spec => {
        let li = document.createElement("li")
        li.innerText = spec
        featuresElement.appendChild(li)
    })
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image
    descriptionElement.innerHTML = laptop.description
    titleElement.innerText = laptop.title
    priceElement.innerText = toCurrency(laptop.price)
}

const handleBuy = () => {
    if(payFromBalance(selectedLaptop.price)){
        alert("You are now a owner of a new laptop!")
    }else{
        alert("You do not have enough money.")
    }
}


laptopsElement.addEventListener("change", handleLaptopStoreChange )
buyElement.addEventListener("click", handleBuy)