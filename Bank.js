

const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const getLoanElement = document.getElementById("getLoan");

let balance = 0;
export let loan = 0;
document.getElementById("loan").hidden = true;

const toCurrency = (value) => {
    return value.toLocaleString('de-DE', { style: 'currency', currency: 'EUR' });
}

balanceElement.innerHTML = toCurrency(balance)
loanElement.innerHTML = toCurrency(loan)

const addToBalance = (newBalance) => {
    balance += newBalance
    balanceElement.innerHTML = toCurrency(balance)
}

const payFromBalance = (price) => {
    if(price > balance){
        alert("You do not have enough money")
        return false
    }else{
        balance -= price
        balanceElement.innerHTML = toCurrency(balance)
        return true
    }
 
}

const payLoan = (amount) => {
    if(loan < amount){
        let remainingPay= (amount-loan)
        console.log(balance)
        console.log(remainingPay)
        balance = balance + remainingPay
        console.log(balance)
        loan = 0;
        loanElement.innerHTML = "Outstanding Loan: " + toCurrency(loan)
        balanceElement.innerHTML = toCurrency(balance)
    }else{
        loan = loan - amount;
        loanElement.innerHTML = "Outstanding Loan: " + toCurrency(loan)
    }

    if(loan === 0){
        document.getElementById("loan").hidden = true; 
    }
}

const handleGetLoan = () => {
    let newLoan = prompt("Enter the loan amout")
    newLoan = newLoan.replace(/,/g, '.') 

    if(isNaN(newLoan) || newLoan <= 0){
        alert("Please enter a number bigger than zero")
    }else if(loan > 0){
        alert("You cannot get a new loan before repaying the current one.")
    }else if(newLoan > (balance*2)){
        alert("You cannot get a bigger loan than double of your current balance.")
    }else{       
        loan=parseFloat(newLoan);
        balance += loan
        loanElement.innerHTML = "Outstanding Loan: " + toCurrency(loan)
        balanceElement.innerHTML = toCurrency(balance)
        document.getElementById("loan").hidden = false;
        document.getElementById("repayLoan").hidden = false;
    }
    
    

}


getLoanElement.addEventListener("click", handleGetLoan)



export {addToBalance, payLoan, toCurrency, payFromBalance}



